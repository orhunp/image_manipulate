package com.op;

public class Main {

    public static void main(String[] args) {
        try {
            new App(args[0], args[1], Operation.valueOf(args[2].toUpperCase()),
                    Integer.parseInt(args[3]), Boolean.parseBoolean(args[4])).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
