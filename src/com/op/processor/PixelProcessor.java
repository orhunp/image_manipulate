package com.op.processor;

import com.op.Operation;

public class PixelProcessor {

    public static byte[] process(byte[] pixels, Operation operation, int percent) {
        switch (operation) {
            case LIGHTER:
                return PixelProcessor.lightenPixels(pixels, percent);
            case DARKER:
                return PixelProcessor.darkenPixels(pixels, percent);
        }
        return null;
    }

    private static byte[] lightenPixels(byte[] pixels, int percent) {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = (byte) (((255 - (pixels[i] & 0xFF)) * percent / 100) + (pixels[i] & 0xFF));
        }
        return pixels;
    }

    private static byte[] darkenPixels(byte[] pixels, int percent) {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = (byte) ((pixels[i] & 0xFF) - ((pixels[i] & 0xFF) * percent / 100));
        }
        return pixels;
    }
}
