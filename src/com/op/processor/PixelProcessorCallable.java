package com.op.processor;

import com.op.Operation;

import java.util.concurrent.Callable;

public class PixelProcessorCallable implements Callable<byte[]> {
    private final byte[] pixels;
    private final Operation operation;
    private final int percent;

    public PixelProcessorCallable(byte[] pixels, Operation operation, int percent) {
        this.pixels = pixels;
        this.operation = operation;
        this.percent = percent;
    }

    public byte[] call() {
        return PixelProcessor.process(pixels, operation, percent);
    }
}
