package com.op;

import com.op.exception.InvalidDimensionsException;
import com.op.image.Image;
import com.op.processor.PixelProcessor;
import com.op.processor.PixelProcessorCallable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class App {
    Image image = new Image();
    File outputFile;
    Operation operation;
    int percent;
    boolean multithreaded;

    public App(String input, String output, Operation operation, int percent, boolean multithreaded)
            throws IOException, InvalidDimensionsException {
        this.operation = operation;
        this.percent = percent;
        this.multithreaded = multithreaded;
        image.readImage(new File(input));
        outputFile = new File(output);
        if (outputFile.exists()) {
            if (!outputFile.delete()) {
                throw new IOException("failed to delete file");
            }
        }
    }

    public void run() throws IOException {
        if (multithreaded) {
            // Get the thread count
            int threadCount = Runtime.getRuntime().availableProcessors() * 4;

            // Split the array into separate arrays for processing
            ArrayList<byte[]> pixelArrays = new ArrayList<>();
            int chunks = (int) Math.ceil((double) image.getPixels().length / threadCount);
            for (int i = 0; i < image.getPixels().length; i += chunks) {
                pixelArrays.add(Arrays.copyOfRange(image.getPixels(), i, Math.min(image.getPixels().length, i + chunks)));
            }

            // Create callables
            List<Callable<byte[]>> callables = new ArrayList<>();
            for (int i = 0; i < pixelArrays.size(); i++) {
                callables.add(new PixelProcessorCallable(pixelArrays.get(i), operation, percent));
            }

            // Run the executor
            ByteArrayOutputStream pixelResults = new ByteArrayOutputStream();
            ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
            try {
                List<Future<byte[]>> results = executorService.invokeAll(callables);
                for (Future<byte[]> result : results) {
                    pixelResults.write(result.get());
                }
            } catch (InterruptedException | ExecutionException ex) {
                ex.printStackTrace();
            } finally {
                executorService.shutdownNow();
            }

            // Set the results
            image.setPixels(pixelResults.toByteArray());
        } else {
            image.setPixels(PixelProcessor.process(image.getPixels(), operation, percent));
        }
        image.saveImage(outputFile);
    }
}
