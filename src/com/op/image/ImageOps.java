package com.op.image;

import com.op.exception.InvalidDimensionsException;

import java.io.File;
import java.io.IOException;

public interface ImageOps {
    void readImage(File inputFile) throws IOException, InvalidDimensionsException;

    void saveImage(File outputFile) throws IOException;
}
