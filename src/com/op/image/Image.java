package com.op.image;

import com.op.exception.InvalidDimensionsException;

import javax.imageio.ImageIO;
import java.awt.image.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class Image implements ImageOps {
    int width, height;
    byte[] pixels;

    public Image(int width, int height, byte[] pixels) {
        this.width = width;
        this.height = height;
        this.pixels = pixels;
    }

    public Image() {
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public byte[] getPixels() {
        return pixels;
    }

    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }

    @Override
    public void readImage(File inputFile) throws IOException, InvalidDimensionsException {
        BufferedImage imageBuffer = ImageIO.read(inputFile);
        this.setWidth(imageBuffer.getWidth());
        this.setHeight(imageBuffer.getHeight());

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(imageBuffer, "jpg", byteArrayOutputStream);
        this.setPixels(((DataBufferByte) imageBuffer.getRaster().getDataBuffer()).getData());

        // Sanity check for RGB pixels
        if (getWidth() * getHeight() * 3 != getPixels().length) {
            throw new InvalidDimensionsException(String.format(Locale.US,
                    "invalid image dimensions: %dx%d (%d pixels)", getWidth(), getHeight(), getPixels().length));
        }
    }

    @Override
    public void saveImage(File outputFile) throws IOException {
        DataBuffer buffer = new DataBufferByte(getPixels(), getPixels().length);
        SampleModel sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE,
                getWidth(), getHeight(), 3, getWidth() * 3, new int[]{2, 1, 0});
        Raster raster = Raster.createRaster(sampleModel, buffer, null);
        BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        bufferedImage.setData(raster);
        ImageIO.write(bufferedImage, "jpg", outputFile);
    }
}
