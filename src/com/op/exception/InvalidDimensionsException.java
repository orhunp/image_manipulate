package com.op.exception;

public class InvalidDimensionsException extends Exception {
    public InvalidDimensionsException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
